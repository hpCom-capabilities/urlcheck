# UrlCheck

> This tool checks all locales of a given master url if they exist or not (in form of: <url-start>/cc/ll/<url-end>) and gives back the result in Http status code groups. Results can also be downloaded as csv.

## User Tips

Hence HP servers are unreliable, the need to retest certain results sometimes manually arose. By pressing the Retest button, you are able to restest results that came back with 3XX, 4XX or 5XX to see if they are certainly 3XX, 4XX or 5XX.

## Build Setup

``` bash
# install dependencies
npm install

# run backend server on localhost:8081
npm run server

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
