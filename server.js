'use strict'

require('dotenv').config()
const express = require('express')
const app = express()
const request = require('request-promise')
const urlModule = require('url')

let bindInterface = process.env.BIND_INTERFACE || '127.0.0.1'

app.disable('x-powered-by')

// Add headers
app.use(function (req, res, next) {
  // Website you wish to allow to connect
  res.setHeader('Access-Control-Allow-Origin', '*')
  // Request methods you wish to allow
  res.setHeader('Access-Control-Allow-Methods', 'GET')
  // Request headers you wish to allow
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type,Pragma,Cache-Control')
  // Pass to next layer of middleware
  next()
})

function checkUrl (url) {
  let result
  let onlyHP = 'Only "www.hp.com" domain is allowed!'
  try {
    result = (urlModule.parse(url).hostname === 'www.hp.com') ? true : onlyHP
  } catch (e) {
    result = onlyHP
  }

  if (url.length === 0 || !(result === true || result === onlyHP)) {
    result = 'Url have to contain "cc-ll" (country code and language code)'
  }

  return result
}

app.get('/check', function (req, res) {
  let url = req.query.url
  let result = checkUrl(req.query.url)

  if (result === true) {
    request({ method: 'GET', url: url, followRedirect: true, simple: false, resolveWithFullResponse: true })
      .then(function (response) {
        const route = {
          finalStatusCode: response.statusCode,
          redirects: response.request._redirect.redirects
        }
        res.send(JSON.stringify(route))
      })
      .catch(function (err) {
        // custom status code, failed for other technical reasons
        res.send(900)
      })
  } else {
    res.status(403).send(result)
  }
})

app.listen(8081, bindInterface || '127.0.0.1', function () {
  console.log(`Server listening on ${bindInterface}:8081.`)
})
